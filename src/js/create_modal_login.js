function createModalLogin() {
    // enterBtn.style.display = 'none';
    const popup = new Modal();
    popup.renderModal(enterBtn);
    popup.addStylesToModal(modalWindow, modalPosition);

    //  ================Создаем форму для регистрации==================================================================
    const loginForm = new Form();
    loginForm.renderForm(popup.modal, formLoginAtr, formClass);

    const emailField = new FormInput(loginForm.form);
    emailField.renderFormInput(inputEmail, inputClassName);

    const passwordField = new FormInput(loginForm.form);
    passwordField.renderFormInput(inputPassword, inputClassName);

    const enter = new FormInput(loginForm.form);
    enter.renderFormInput(inputEnter, btnFormClass);

    loginForm.form.addEventListener('submit', (e) => {
        e.preventDefault();
        getToken(urlLogin,urlCard ,popup.modal, emailField.getValue(), passwordField.getValue(), 'token', enterBtn, btnCreateVisit);
        // getCards("https://ajax.test-danit.com/api/v2/cards", sessionStorage.getItem('token'), 'cardsList');
        
    });
}

