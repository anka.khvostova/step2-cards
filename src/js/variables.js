

// ================Для всех инпутов стили===========================================================

// ----------Селект для выбора врача---------------------------------------------------------------
const doctorSelect = {
    id: 'doctorSelect',
    required: 'true',
    name: 'doctor_card',
    form: 'formCardCreate',
}
const selectComponents = {
    classNameLabel: "label",
    labelText: "Выбрать врача",
    options: [['no_doctor', "не выбрано"], ['cardio', "Кардиолог"], ['terapevt', "Терапевт"], ['dentist', "Стоматолог"]],
};
const formAtr = {
    id: 'formCardCreate',
    name: 'form-card',
};
const inputsForCard = {
    formAtr: {
        id: 'formCardCreate',
        name: 'form-card',
    },
    prioritySelect: {
        id: 'ugrentlySelect',
        required: 'true',
        name: 'doctor_card',
        form: 'formCardCreate',
    },
    selectPriority: {
        classNameLabel: "label",
        labelText: "Срочность",
        options: [['low', "Обычная", 'true'], ['normal', "Приоритетная"], ['high', "Неотложная"]],
    },
    // ------------Input for card--------------------------------------------------------------------------------

    inputName: {
        type: 'text',
        required: 'true',
        placeholder: 'ФИО',
        name: 'patient_name',
    },
    inputVisitPurpose: {
        type: 'text',
        required: 'true',
        placeholder: 'Цель визита',
        name: 'visit_purpose',
    },

    inputVisitAge: {
        type: 'number',
        required: 'true',
        placeholder: 'Возраст',
        name: 'age',
    },
    inputVisitPressure: {
        type: 'number',
        required: 'true',
        placeholder: 'Кровеносное давление',
        name: 'blood_pressure',
    },
    inputVisitMass: {
        type: 'number',
        required: 'true',
        placeholder: 'Индекс массы тела',
        name: 'index_mass',
    },
    inputVisitLast: {
        type: 'date',
        required: 'true',
        placeholder: 'Дата последнего визита',
        name: 'last-visit',
    },
    submitFormBtn: {
        type: 'submit',
        id: 'create_form',
        value: 'Создать карточку',
        name: 'submit_btn',
    },
    resetFormBtn: {
        type: 'reset',
        id: 'reset_form',
        value: 'Очистить карточку',
        name: 'reset_btn',
    },
    textareaDescription: {
        id: 'description',
        name: 'description',
        placeholder: 'Краткое описание',
        // required: 'true',
        rows: 5,
    },
    textareaIllnesses: {
        id: 'illnesses',
        name: 'illnesses',
        placeholder: 'Ранее перенесенные заболевания',
        // required: 'true',
        rows: 5,
    },
}
// ------------Input for registration--------------------------------------------------
const formLoginAtr = {
    id: 'form_login',
    name: 'form-login',
}
const inputEmail = {
    type: 'email',
    required: 'true',
    placeholder: 'Введите e-mail',
    name: 'email',
};
const inputPassword = {
    type: 'password',
    required: 'true',
    placeholder: 'Введите пароль',
    name: 'password',
};
const inputEnter = {
    type: 'submit',
    id: 'create_form',
    value: 'Войти',
    name: 'submit_btn',
};

// ------------Classes styles----------------------------------------------------------
const stylesForm = {
    inputClassName: 'form-input',
    btnFormClass: 'form-btn',
    formClass: 'cards-create-form',
};

const formClass = 'cards-create-form';
const inputClassName = 'form-input';
const btnFormClass = 'form-btn';
// -----------------------

const modalWindow = {
    modalClass: 'login-modal',
    modalId: '2',
    btnClass: 'login-modal-btn',
    btnId: '4',
    btnCloseText: 'X',

};
const modalPosition = {
    top: '2%',
    right: '5%',
}
// -------------------------------------------------------------
const modalWindow2 = {
    modalClass: 'card-modal',
    modalId: '2',
    btnClass: 'login-modal-btn',
    btnId: '4',
    btnCloseText: 'X',
};

const modalPositionCard = {
    top: '25%',
    left: '30%',
    right: '30%',
}
// ---------------------------------------------------------------

const sendCardInfo = {
    url:'https://ajax.test-danit.com/api/v2/cards',
    method:'POST',
    sessionStorKey:'newCard',
};