class Form {
    constructor() {
        this.forma = document.createElement('form');
    }
 
    // ----------------------
    addAttributes(atributes, domElement, className) {
        const atributesInElement = Object.entries(atributes);
        for (let [atr, value] of atributesInElement) {
            domElement.setAttribute(atr,value);
        }
        domElement.classList.add(className);
    }
    // ----------------------
    createForm(atributes, className,parentNode) {

        this.addAttributes(atributes, this.forma, className);
       parentNode.append(this.forma);
    }
    // ----------------------
    createSelect({ classNameLabel, labelText, options }, atributes, className) {
        this.select = document.createElement('select');
        this.selectLabel = document.createElement('label');
        this.addAttributes(atributes, this.select, className);
        this.selectLabel.for = this.select.id;
        this.selectLabel.classList.add = classNameLabel;
        this.selectLabel.innerText = labelText;
        for (let [id, text, selected] of options) {
            let option = document.createElement('option');
            option.id = id;
            option.value = id;
            option.innerText = text;
            option.selected = selected;
            this.select.append(option);
        }
        this.forma.append(this.selectLabel, this.select);
    }
    // ----------------------
    createInput(atributes, className) {
        this.input = document.createElement('input');
     
        this.addAttributes(atributes, this.input, className);
        this.forma.append(this.input);
    }
    // ----------------------
    createTextArea(atributes, className) {
        this.textarea = document.createElement('textarea');
     
        this.addAttributes(atributes, this.textarea, className);
        this.forma.append(this.textarea);
    }
}

