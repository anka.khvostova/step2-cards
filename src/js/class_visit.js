class Visit {
    constructor({ id, fullname, ugrency,
        visitPurpose, description }) {
        this.id = id;
        this.fullname = fullname;
        this.ugrency = ugrency;
        this.visitPurpose = visitPurpose;
        this.description = description;
        this.form = new Form();

    }

    renderVisitForm(container, cardInfo, { formClass, inputClassName, btnFormClass }, { formAtr, submitFormBtn, resetFormBtn, inputName, prioritySelect, selectPriority, inputVisitPurpose, textareaDescription }) {
        this.form.renderForm(container, formAtr, formClass);
        this.patientName = new FormInput(this.form.form);
        this.patientName.renderFormInput(inputName, inputClassName);
        this.selectUgrency = new FormSelect(this.form.form);
        this.selectUgrency.renderFormSelect(prioritySelect, inputClassName, selectPriority);
        this.visitPurp = new FormInput(this.form.form);
        this.visitPurp.renderFormInput(inputVisitPurpose, inputClassName);
        this.descrip = new FormTextArea(this.form.form);
        this.descrip.renderFormTextarea(textareaDescription, inputClassName);
        const submBtn = new FormInput(this.form.form);
        submBtn.renderFormInput(submitFormBtn, btnFormClass);
        const reset = new FormInput(this.form.form);
        reset.renderFormInput(resetFormBtn, btnFormClass);
        const parentContainer = container;


        this.form.form.addEventListener('submit', (e) => {
            e.preventDefault();
            this.getValues();

            parentContainer.remove();
            delete this.form;
            this.sendToServer(cardInfo);
        });
    }
    getValues() {
        this.fullname = this.patientName.getValue();
        this.visitPurpose = this.visitPurp.getValue();
        this.description = this.descrip.getValue();
        this.ugrency = this.selectUgrency.getValue();
        delete this.patientName;
        delete this.visitPurp;
        delete this.descrip;
        delete this.selectUgrency;
    }

    sendToServer({ url, method, sessionStorKey }) {
        const token = sessionStorage.getItem('token');
        fetch(url, {
            method: method,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify(this)
        })
            .then(response => response.json())
            .then(response => {
                console.log(response)
                sessionStorage.setItem(sessionStorKey, JSON.stringify(response))
            })
    }

    renderToBoard(container, classStyle, classCard, className, classDoctor, classBtn) {
        this.card = document.createElement('div');
        this.card.classList.add(classCard);
        const name = document.createElement('p');
        const doctor = document.createElement('p');
        name.classList.add(className);
        doctor.classList.add(classDoctor);
        name.textContent = this.fullname;
        doctor.textContent = this.title;
        const btn = document.createElement('button');
        btn.classList.add(classBtn);
        btn.innerText = 'Показать больше';
        container.append(this.card);
        this.card.append(name, doctor, btn);
        // -------------------------------------------
        const rollUp = document.createElement('button');
        rollUp.style.display = 'none';
        rollUp.classList.add(classBtn);
        rollUp.textContent = 'Свернуть';
        const ugrency = document.createElement('p');
        ugrency.classList.add(classStyle);
        ugrency.textContent = this.ugrency;
        const purpose = document.createElement('p');
        purpose.classList.add(classStyle);
        purpose.textContent = this.visitPurpose;
        const description = document.createElement('p');
        description.textContent = this.description;
        description.classList.add(classStyle);
        const del = document.createElement('button');
        const edit = document.createElement('button');
        del.textContent = "Удалить карточку";
        edit.textContent = "Редактировать";
        del.classList.add(classBtn);
        edit.classList.add(classBtn);
        this.card.append(rollUp, ugrency, purpose, description, del, edit);

        btn.addEventListener('click', () => {

        })
    }


}
// -------------------------------------------------------------------------------
class VisitDentist extends Visit {
    constructor({ id, fullname, ugrency, visitPurpose,
        description, lastVisitData }) {
        super({ id, fullname, ugrency, visitPurpose, description });
        this.doctor = 'Стоматолог';
        this.title = 'Визит к стоматологу';
        this.lastVisitData = lastVisitData;
    }
    renderVisitForm(container, cardInfo, { formClass, inputClassName, btnFormClass }, { formAtr, submitFormBtn, resetFormBtn, inputName, prioritySelect, selectPriority, inputVisitPurpose, textareaDescription, inputVisitLast }) {
        super.renderVisitForm(container, cardInfo, { formClass, inputClassName, btnFormClass }, { formAtr, submitFormBtn, resetFormBtn, inputName, prioritySelect, selectPriority, inputVisitPurpose, textareaDescription });
        this.lastVisit = new FormInput(this.form.form);
        this.lastVisit.renderFormInput(inputVisitLast, inputClassName);

    }
    getValues() {
        super.getValues();
        this.lastVisitData = this.lastVisit.getValue();
        console.log(this.lastVisitData);
        delete this.lastVisit;
    }

}
// ------------------------------------------------------------------------------------
class VisitCardiologist extends Visit {
    constructor({ id, fullname, ugrency, visitPurpose, description,
        age, pressure, bodyMassIndex, pastIllnesses }) {
        super({ id, fullname, ugrency, visitPurpose, description });
        this.doctor = 'Кардиолог';
        this.title = 'Визит к кардиологу';
        this.age = age;
        this.pressure = pressure;
        this.bodyMassIndex = bodyMassIndex;
        this.pastIllnesses = pastIllnesses;
    }
    renderVisitForm(container, cardInfo, { formClass, inputClassName, btnFormClass }, { formAtr, submitFormBtn, resetFormBtn, inputName, prioritySelect, selectPriority, inputVisitPurpose, textareaDescription, inputVisitAge, inputVisitMass, inputVisitPressure, textareaIllnesses }) {
        super.renderVisitForm(container, cardInfo, { formClass, formClass, inputClassName, btnFormClass }, { formAtr, submitFormBtn, resetFormBtn, inputName, prioritySelect, selectPriority, inputVisitPurpose, textareaDescription });
        this.patientage = new FormInput(this.form.form);
        this.patientage.renderFormInput(inputVisitAge, inputClassName);
        this.mass = new FormInput(this.form.form);
        this.mass.renderFormInput(inputVisitMass, inputClassName);
        this.bloodPressure = new FormInput(this.form.form);
        this.bloodPressure.renderFormInput(inputVisitPressure, inputClassName);
        this.illnesses = new FormTextArea(this.form.form);
        this.illnesses.renderFormTextarea(textareaIllnesses, inputClassName);

    }
    getValues() {
        super.getValues();
        this.age = this.patientage.getValue();
        this.pressure = this.bloodPressure.getValue();
        this.bodyMassIndex = this.mass.getValue();
        this.pastIllnesses = this.illnesses.getValue();
        delete this.patientage;
        delete this.illnesses;
        delete this.bloodPressure;
        delete this.mass;
    }
}
// -------------------------------------------------------------------------------------------
class VisitTherapist extends Visit {
    constructor({ id, fullname, ugrency,
        visitPurpose, description, age }) {
        super({ id, fullname, ugrency, visitPurpose, description });
        this.doctor = 'Терапевт';
        this.title = 'Визит к терапевту';
        this.age = age;
    }
    renderVisitForm(container, cardInfo, { formClass, inputClassName, btnFormClass }, { formAtr, submitFormBtn, resetFormBtn, inputName, prioritySelect, selectPriority, inputVisitPurpose, textareaDescription, inputVisitAge }) {
        super.renderVisitForm(container, cardInfo, { formClass, inputClassName, btnFormClass }, { formAtr, submitFormBtn, resetFormBtn, inputName, prioritySelect, selectPriority, inputVisitPurpose, textareaDescription });
        this.patientage = new FormInput(this.form.form);
        this.patientage.renderFormInput(inputVisitAge, inputClassName);
    }
    getValues() {
        super.getValues();
        this.age = this.patientage.getValue();
        delete this.patientage;
    }

}

