function getToken(url,urlCard, modalWindow, emailValue, passValue, keyName, buttonEnter, buttonCreateVisit) {

  fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({ email: emailValue, password: passValue })
  })
    .then(response => response.text())
    .then(token => {
      console.log(token);
      sessionStorage.setItem(keyName, token);
      modalWindow.remove();
      buttonEnter.remove();
      buttonCreateVisit.style.display = 'inline';
      getCards(urlCard, sessionStorage.getItem(keyName),'cardsList') 
    })

}

// -----------------------------------------------------------------
function sendCard(url, token, card, sessionStorKey) {
  fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    body: JSON.stringify(card)
  })
    .then(response => response.json())
    .then(response => {
      console.log(response)
      sessionStorage.setItem(sessionStorKey, JSON.stringify(response))
    }
    )
}

// ----------------------------------------------------------------
function getCards(url, token, sessionStorKey) {
  fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },

  })
    .then(response => response.json())
    .then(response => {
      // console.log(response);
      sessionStorage.setItem(sessionStorKey, JSON.stringify(response));
      const cardsList = JSON.parse(sessionStorage.getItem(sessionStorKey));
      console.log(cardsList);
      for (let card of cardsList) {
        // console.log(card);
        if (card.doctor === 'Терапевт') {
          const boardCard = new VisitTherapist(card);
          boardCard.renderToBoard(cardBox);

        }
        else if (card.doctor == 'Стоматолог') {
          const boardCard = new VisitDentist(card);
          boardCard.renderToBoard(cardBox);

        }
        else if (card.doctor == 'Кардиолог') {
          const boardCard = new VisitCardiologist(card);
          boardCard.renderToBoard(cardBox);
        }

      }
    })
}
 