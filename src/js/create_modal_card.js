// ---------Создаем модальое окно с формой для карточки---------------------------------------------------------------------------------
function createModalFormCard() {
    const popup = new Modal();
    popup.renderModal(btnCreateVisit);
    popup.addStylesToModal(modalWindow2, modalPositionCard);
    // =======================Создаём форму для карточки====================================

    const selectDoctor = new FormSelect(popup.modal);
    selectDoctor.renderFormSelect(doctorSelect, inputClassName, selectComponents);


    // -------Добавляем обработчик на селект для врача-----------------------------------------------------------------------------------


    selectDoctor.select.addEventListener('change', function () {
        const form = popup.modal.querySelector('form');
        if (form) {
            form.remove();
        }

        let visit;
        const target = this.options[this.selectedIndex].value;
        if (target === 'cardio') {
            doctor = target;
            visit = new VisitCardiologist({});
            
            visit.renderVisitForm(popup.modal,sendCardInfo, stylesForm, inputsForCard);
        }
        else if (target === 'dentist') {
            doctor = target;
            visit = new VisitDentist({});
           
            visit.renderVisitForm(popup.modal,sendCardInfo, stylesForm, inputsForCard);

        }
        else if (target === 'terapevt') {
            doctor = target;
            visit = new VisitTherapist({});
           
            visit.renderVisitForm(popup.modal,sendCardInfo, stylesForm, inputsForCard);
            
        }

        //     const token = sessionStorage.getItem('token');
        //     myToken = token;
        //     console.log(sessionStorage.card);
        //     console.log(cardDone);
        //     popup.modal.remove();
        //     btnCreateVisit.style.display = 'inline';
        //     sendCard(urlCard, token, cardDone, keyForNewCard);
    })
}
// ------------------------Обработчик события на кнопку сабмита для создания объекта---------------------------------------------------------------------------------
// let doctor;
// let cardDone = {};
// form.form.addEventListener('submit', function (e) {
//     e.preventDefault();
//     if (doctor === 'cardio') {
//         cardDone = new VisitCardiologist(patientName.getValue(), selectUgrency.getValue(), visitPurpose.getValue(), description.getValue(), age.getValue(), bloodPressure.getValue(), mass.getValue(), illnesses.getValue());
//     }
//     else if (doctor === 'terapevt') {
//         cardDone = new VisitTherapist(patientName.getValue(), selectUgrency.getValue(), visitPurpose.getValue(), description.getValue(), age.getValue())
//     }
//     else if (doctor === 'dentist') {
//         cardDone = new VisitDentist(patientName.getValue(), selectUgrency.getValue(), visitPurpose.getValue(), description.getValue(), lastVisit.getValue());
//     }

//     sessionStorage.setItem('card', JSON.stringify(cardDone));
//     const token = sessionStorage.getItem('token');
//     myToken = token;
//     console.log(sessionStorage.card);
//     console.log(cardDone);
//     popup.modal.remove();
//     btnCreateVisit.style.display = 'inline';
//     sendCard(urlCard, token, cardDone, keyForNewCard);
// });


