class Form {
    constructor() {
        this.form = document.createElement('form');
    }

    addAttributes(atributes, domElement, className) {
        const atributesInElement = Object.entries(atributes);
        for (let [atr, value] of atributesInElement) {
            domElement.setAttribute(atr, value);
        }
        domElement.classList.add(className);
    }

    renderForm(parentNode, atributes, className) {
        this.addAttributes(atributes, this.form, className);
        parentNode.append(this.form);
    }
}
// ----------------------------------------------------------------------------------------
class FormSelect extends Form {
    constructor(form) {
        super();
        this.form = form;
        this.select = document.createElement('select');
        this.selectLabel = document.createElement('label');
    }

    renderFormSelect(atributes, className, { classNameLabel, labelText, options }) {
        super.addAttributes(atributes, this.select, className);

        this.selectLabel.for = this.select.id;
        this.selectLabel.classList.add = classNameLabel;
        this.selectLabel.innerText = labelText;
        for (let [id, text, selected] of options) {
            let option = document.createElement('option');
            // option.id = id;
            option.value = id;
            option.innerText = text;
            option.selected = selected;
            this.select.append(option);
        }
        this.form.append(this.selectLabel, this.select);
    }
    invisible() {
        this.select.removeAttribute('required');
        this.selectLabel.style.display = 'none';
        this.select.style.display = 'none';

    }
    visible() {
        this.select.style.display = 'inline';
        this.selectLabel.style.display = 'inline';
        this.select.required = 'true';
    }
    getValue() {
        return this.select.options[this.select.selectedIndex].innerText;
    }

}
// ----------------------------------------------------------------------------------------
class FormInput extends Form {
    constructor(form) {
        super();
        this.form = form;
        this.input = document.createElement('input');
    }
    renderFormInput(atributes, className) {
        super.addAttributes(atributes, this.input, className);
        this.form.append(this.input);
    }
    invisible() {
        this.input.removeAttribute('required');
        this.input.style.display = 'none';

    }
    visible() {
        this.input.style.display = 'inline';
        this.input.required = 'true';
    }
    getValue() {
        return this.input.value;
    }
}
// ----------------------------------------------------------------------------------------
class FormTextArea extends Form {
    constructor(form) {
        super();
        this.form = form;
        this.textarea = document.createElement('textarea');
    }
    renderFormTextarea(atributes, className) {
        super.addAttributes(atributes, this.textarea, className);
        this.form.append(this.textarea);
    }
    invisible() {
        this.textarea.removeAttribute('required');
        this.textarea.style.display = 'none';

    }
    visible() {
        this.textarea.style.display = 'inline';
        // this.textarea.required = 'true';
    }
    getValue() {
        return this.textarea.value;
    }
}


